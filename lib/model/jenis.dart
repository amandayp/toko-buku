import 'package:toko/toko.dart';
import 'buku.dart';

class Jenis extends ManagedObject<_Jenis> implements _Jenis {

}

class _Jenis{
  @primaryKey
  int id;
  String nama;

  ManagedSet<Buku> bukus;
}