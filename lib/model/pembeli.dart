import 'package:toko/model/buku.dart';
import 'package:toko/model/pembeli_detail.dart';
import 'package:toko/toko.dart';

class Pembeli extends ManagedObject<_Pembeli> implements _Pembeli {

}

class _Pembeli{
  @primaryKey
  int id;
  String nama;

  ManagedSet<DetailPembeli> details;
}