import 'package:toko/toko.dart';
import 'buku.dart';
import 'penulis_daftar.dart';

class Penulis extends ManagedObject<_Penulis> implements _Penulis {

}

class _Penulis{
  @primaryKey
  int id;
  String judul;

  ManagedSet<PenulisDaftar> penulisDaftars;
}