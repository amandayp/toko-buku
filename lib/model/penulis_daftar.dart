import 'package:toko/toko.dart';
import 'buku.dart';
import 'penulis.dart';

class PenulisDaftar extends ManagedObject<_PenulisDaftar> implements _PenulisDaftar {

}

class _PenulisDaftar{
  @primaryKey
  int id;

  @Relate(#penulisDaftars)
  Penulis penulis;

  @Relate(#penulisDaftars)
  Buku buku;
}