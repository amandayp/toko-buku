import 'package:toko/model/pembeli.dart';
import 'package:toko/toko.dart';

import 'buku.dart';

class DetailPembeli extends ManagedObject<_PembeliDetail> implements _PembeliDetail {
  @Serialize()
  //double get total => jumlahBeli * buku.harga;
  double total;

}

class _PembeliDetail{
  @primaryKey
  int id;
  int jumlahBeli;

  @Relate(#details)
  Buku buku;

  @Relate(#details)
  Pembeli pembeli;
}