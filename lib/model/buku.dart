import 'package:toko/model/pembeli_detail.dart';
import 'package:toko/toko.dart';
import 'jenis.dart';
import 'penulis.dart';
import 'penulis_daftar.dart';

class Buku extends ManagedObject<_Buku> implements _Buku {

}

class _Buku{
  @primaryKey
  int id;
  String judul;
  double harga;
  int stok;

  @Relate(#bukus) //fk
  Jenis jenis;

  ManagedSet<DetailPembeli> details;

  ManagedSet<PenulisDaftar> penulisDaftars;
}