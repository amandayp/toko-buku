import 'package:toko/toko.dart';
import 'package:toko/model/penulis_daftar.dart';

class PenulisDaftarController extends ResourceController{
  PenulisDaftarController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getPenulisDaftarList() async{
    final penulisdaftarlist = await Query<PenulisDaftar>(context).fetch();
    return Response.ok(penulisdaftarlist);
  }

  @Operation.get('id')
  Future<Response> getPenulisDaftarById(@Bind.path('id') int id) async{
    final penulisdaftarPB = Query<PenulisDaftar>(context)
    ..where((b) => b.penulis.id).equalTo(id);

    final penulisdaftar = await penulisdaftarPB.fetchOne();

    return Response.ok(penulisdaftar);
  }

  @Operation.post()
  Future<Response> addPenulisDaftar(@Bind.body() PenulisDaftar newPenulisDaftar) async{
    await context.insertObject(newPenulisDaftar);

    return Response.ok('add');
  }

  @Operation.put('id')
  Future<Response> editPenulisDaftar(@Bind.path('id') int id, @Bind.body() PenulisDaftar editPenulisDaftar) async{
    final updatePD = Query<PenulisDaftar>(context)
    ..values = editPenulisDaftar
    ..where((b) => b.id).equalTo(id);

    await updatePD.updateOne();

    return Response.ok('edit');
  }

  @Operation.delete('id')
  Future<Response> deletePenulisDaftar(@Bind.path('id') int id) async{
    final deletePD = Query<PenulisDaftar>(context)
    ..where((b) => b.id).equalTo(id);

    await deletePD.delete();
    return Response.ok('delete');
  }
}