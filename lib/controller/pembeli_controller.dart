import 'package:toko/toko.dart';
import 'package:toko/model/pembeli.dart';

class PembeliController extends ResourceController{
  PembeliController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getPembeliList() async{
    final pembelilist = await Query<Pembeli>(context).fetch();
    return Response.ok(pembelilist);
  }

  @Operation.get('id')
  Future<Response> getPembeliById(@Bind.path('id') int id) async{
    final pembeliP = Query<Pembeli>(context)
    ..where((b) => b.id).equalTo(id);

    final pembeli = await pembeliP.fetchOne();

    return Response.ok(pembeli);
  }

  @Operation.post()
  Future<Response> addPembeli(@Bind.body() Pembeli newPembeli) async{
    await context.insertObject(newPembeli);

    return Response.ok('add');
  }

  @Operation.put('id')
  Future<Response> editPembeli(@Bind.path('id') int id, @Bind.body() Pembeli editPembeli) async{
    final updateP = Query<Pembeli>(context)
    ..values = editPembeli
    ..where((p) => p.id).equalTo(id);

    await updateP.updateOne();

    return Response.ok('edit');
  }

  @Operation.delete('id')
  Future<Response> deletePembeli(@Bind.path('id') int id) async{
    final deleteP = Query<Pembeli>(context)
    ..where((p) => p.id).equalTo(id);

    await deleteP.delete();
    return Response.ok('delete');
  }
}