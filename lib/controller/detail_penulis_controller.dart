import 'package:toko/toko.dart';
import 'package:toko/model/penulis_daftar.dart';

class DetailPenulisrController extends ResourceController{
  DetailPenulisrController(this.context);

  final ManagedContext context;

  @Operation.get('id')
  Future<Response> getPenulisDaftarById(@Bind.path('id') int id) async{
    final detailQ = Query<PenulisDaftar>(context)
    ..where((x) => x.penulis.id).equalTo(id)
    ..returningProperties((x) => [x.id, x.penulis])
    ..join(object: (x) => x.penulis).returningProperties((x) => [x.id, x.judul]);//yg ditampilkan
    //..join(object: (x) => x.pembeli);

    final detail = await detailQ.fetch();
    return Response.ok(detail);
  }
}