import 'package:toko/toko.dart';
import 'package:toko/model/buku.dart';

class BukuController extends ResourceController{
  BukuController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getBukuList() async{
    final bukulist = await Query<Buku>(context).fetch();
    return Response.ok(bukulist);
  }

  @Operation.get('id')
  Future<Response> getBukuById(@Bind.path('id') int id) async{
    final bukuB = Query<Buku>(context)
    ..where((b) => b.id).equalTo(id);

    final buku = await bukuB.fetchOne();

    return Response.ok(buku);
  }

  @Operation.post()
  Future<Response> addBuku(@Bind.body() Buku newBuku) async{
    await context.insertObject(newBuku);

    return Response.ok('add');
  }

  @Operation.put('id')
  Future<Response> editBuku(@Bind.path('id') int id, @Bind.body() Buku editBuku) async{
    final updateB = Query<Buku>(context)
    ..values = editBuku
    ..where((b) => b.id).equalTo(id);

    await updateB.updateOne();

    return Response.ok('edit');
  }

  @Operation.delete('id')
  Future<Response> deleteBuku(@Bind.path('id') int id) async{
    final deleteB = Query<Buku>(context)
    ..where((b) => b.id).equalTo(id);

    await deleteB.delete();
    return Response.ok('delete');
  }
}