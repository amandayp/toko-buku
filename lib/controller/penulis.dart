import 'package:toko/toko.dart';
import 'package:toko/model/penulis.dart';

class PenulisController extends ResourceController{
  PenulisController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getPenulisList() async{
    final penulislist = await Query<Penulis>(context).fetch(); // mengambil semua
    return Response.ok(penulislist);
  }

  @Operation.get('id')
  Future<Response> getPenulisById(@Bind.path('id') int id) async{
    final penulisP = Query<Penulis>(context)
    ..where((p) => p.id).equalTo(id);

    final penulis = await penulisP.fetchOne(); //mengambil satu
    return Response.ok(penulis);
  }

  @Operation.post()
  Future<Response> addPenulis(@Bind.body() Penulis newPenulis) async{
    await context.insertObject(newPenulis);
    
    return Response.ok('add');
  }

  @Operation.put('id')
  Future<Response> editPenulis(@Bind.path('id') int id, @Bind.body() Penulis editPenulis) async{
    final updateP = Query<Penulis>(context)
    ..values = editPenulis
    ..where((p) => p.id).equalTo(id);

    await updateP.updateOne();
    return Response.ok('edit');
  }

  @Operation.delete('id')
  Future<Response> deletePenulis(@Bind.path('id') int id) async{
    final deleteP = Query<Penulis>(context)
    ..where((p) => p.id).equalTo(id);

    await deleteP.delete();
    return Response.ok('delete');
  }
}