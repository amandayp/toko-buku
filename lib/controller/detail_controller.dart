import 'package:toko/toko.dart';
import 'package:toko/model/pembeli_detail.dart';

class DetailController extends ResourceController{
  DetailController(this.context);

  final ManagedContext context;

  @Operation.get('id')
  Future<Response> getPembeliById(@Bind.path('id') int id) async{
    final detailQ = Query<DetailPembeli>(context)
    ..where((x) => x.pembeli.id).equalTo(id)
    ..returningProperties((x) => [x.id, x.buku, x.jumlahBeli])
    ..join(object: (x) => x.buku).returningProperties((x) => [x.id, x.judul, x.harga]);//yg ditampilkan
    //..join(object: (x) => x.pembeli);

    final detail = await detailQ.fetch();

    double totalTemenan = 0;

    detail.forEach((x){
      x.total = x.jumlahBeli * x.buku.harga;
      totalTemenan = totalTemenan + x.total;

    });
    return Response.ok(detail);
  }
}