import 'package:toko/toko.dart';
import 'package:toko/model/jenis.dart';

class JenisController extends ResourceController{
  JenisController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getjenisList() async{
    final jenislist = await Query<Jenis>(context).fetch();
    return Response.ok(jenislist);
  }

  @Operation.get('id')
  Future<Response> getJenisById(@Bind.path('id') int id) async{
    final jenisJ = Query<Jenis>(context)
    ..where((b) => b.id).equalTo(id);

    final jenis = await jenisJ.fetchOne();

    return Response.ok(jenis);
  }

  @Operation.post()
  Future<Response> addJenis(@Bind.body() Jenis newJenis) async{
    await context.insertObject(newJenis);

    return Response.ok('add');
  }

  @Operation.put('id')
  Future<Response> editJenis(@Bind.path('id') int id, @Bind.body() Jenis editJenis) async{
    final updateJ = Query<Jenis>(context)
    ..values = editJenis
    ..where((j) => j.id).equalTo(id);

    await updateJ.updateOne();

    return Response.ok('edit');
  }

  @Operation.delete('id')
  Future<Response> deleteJenis(@Bind.path('id') int id) async{
    final deleteJ = Query<Jenis>(context)
    ..where((j) => j.id).equalTo(id);

    await deleteJ.delete();
    return Response.ok('delete');
  }
}