import 'package:toko/toko.dart';
import 'package:toko/model/pembeli_detail.dart';

class DetailPembeliController extends ResourceController{
  DetailPembeliController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getDetailPembeliList() async{
    final detailpembelillist = await Query<DetailPembeli>(context).fetch();
    return Response.ok(detailpembelillist);
  }

  @Operation.get('id')
  Future<Response> getDetailPembeliById(@Bind.path('id') int id) async{
    final pembelidetailPB = Query<DetailPembeli>(context)
    ..where((b) => b.id).equalTo(id);

    final detailpembeli = await pembelidetailPB.fetchOne();

    return Response.ok(detailpembeli);
  }

  @Operation.post()
  Future<Response> addDetailPembeli(@Bind.body() DetailPembeli newDetailPembeli) async{
    await context.insertObject(newDetailPembeli);

    return Response.ok('add');
  }

  @Operation.put('id')
  Future<Response> editPembeliDetail(@Bind.path('id') int id, @Bind.body() DetailPembeli editDetailPembeli) async{
    final updatePB = Query<DetailPembeli>(context)
    ..values = editDetailPembeli
    ..where((b) => b.id).equalTo(id);

    await updatePB.updateOne();

    return Response.ok('edit');
  }

  @Operation.delete('id')
  Future<Response> deleteDetailPembeli(@Bind.path('id') int id) async{
    final deletePB = Query<DetailPembeli>(context)
    ..where((b) => b.id).equalTo(id);

    await deletePB.delete();
    return Response.ok('delete');
  }
}
