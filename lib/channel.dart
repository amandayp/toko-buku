import 'package:toko/controller/buku_controller.dart';
import 'package:toko/controller/jenis_controller.dart';
import 'package:toko/controller/pembeli_controller.dart';
import 'package:toko/controller/detail_controller.dart';

import 'controller/detail_penulis_controller.dart';
import 'controller/pembeli_detail_controller.dart';
import 'controller/penulis.dart';
import 'controller/penulis_daftar.dart';
import 'toko.dart';

/// This type initializes an application.
///
/// Override methods in this class to set up routes and initialize services like
/// database connections. See http://aqueduct.io/docs/http/channel/.
class TokoChannel extends ApplicationChannel {
  ManagedContext context;

  @override
  Future prepare() async {
    logger.onRecord.listen((rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));
    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final persistentStore = PostgreSQLPersistentStore.fromConnectionInfo("postgres", "United99", "localhost", 5432, "buku");

    context = ManagedContext(dataModel, persistentStore);
  }

  /// Construct the request channel.
  ///
  /// Return an instance of some [Controller] that will be the initial receiver
  /// of all [Request]s.
  ///
  /// This method is invoked after [prepare].
  @override
  Controller get entryPoint {
    final router = Router();

    // Prefer to use `link` instead of `linkFunction`.
    // See: https://aqueduct.io/docs/http/request_controller/
    router
      .route("/buku/[:id]")
      ..link(() => BukuController(context));

    router
      .route("/jenis/[:id]")
      ..link(() => JenisController(context));

    router
      .route("/pembeli/[:id]")
      ..link(() => PembeliController(context));

    router
      .route("/pembeli/:id/detail")
      ..link(() => DetailController(context));

    router
      .route("/detailpembeli/[:id]")
      ..link(() => DetailPembeliController(context));

    router
      .route("/penulis/[:id]")
      ..link(() => PenulisController(context));

    router
      .route("/penulis/:id/detail")
      ..link(() => DetailPenulisrController(context));

    router
      .route("/penulisdaftar/[:id]")
      ..link(() => PenulisDaftarController(context));

    return router;
  }
}