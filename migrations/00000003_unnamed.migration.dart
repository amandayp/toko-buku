import 'dart:async';
import 'package:aqueduct/aqueduct.dart';   

class Migration3 extends Migration { 
  @override
  Future upgrade() async {
   		database.deleteColumn("_PembeliDetail", "idPembeli");
		database.deleteColumn("_PembeliDetail", "idBuku");
  }
  
  @override
  Future downgrade() async {}
  
  @override
  Future seed() async {}
}
    